package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while (true) {
            System.out.printf("Let's play round %d", roundCounter);

            // computer move
            Random rand = new Random();
            int index = rand.nextInt(rpsChoices.size()); 
            String computerChoice = rpsChoices.get(index);

            // player move
            String humanChoice;
            // checks if input is valid
            while (true) {
                System.out.printf("\nYour choice (Rock/Paper/Scissors)?\n");
                humanChoice = sc.nextLine().toLowerCase();
                if (rpsChoices.contains(humanChoice)) {
                    break;
                } else {
                    System.out.printf("I do not understand %s. Could you try again?", humanChoice);
                }
            }
            // prints result
            if (humanChoice.equals(computerChoice)) {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!", humanChoice, computerChoice);
            } else if (isWinner(humanChoice, computerChoice)) {
                System.out.printf("Human chose %s, computer chose %s. Human wins!", humanChoice, computerChoice);
                humanScore += 1;
            } else {
                System.out.printf("Human chose %s, computer chose %s. Computer wins!", humanChoice, computerChoice);
                computerScore += 1;
            }
            
            // score
            System.out.printf("\nScore: human %d, computer %d", humanScore, computerScore);
            
            // continue?
            String cont;
            while (true) {
                System.out.printf("\nDo you wish to continue playing? (y/n)?\n");
                cont = sc.nextLine().toLowerCase();
                if (cont.equals("y") || cont.equals("n")){
                    break;
                } else {
                    System.out.printf("I don't understand %s. Try again", cont);
                }
            }
            if (cont.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter += 1;
        }
    }

    // Win condition
    static Boolean isWinner(String humanChoice, String computerChoice) {
        if (humanChoice.equals("rock")) {
            return computerChoice.equals("scissors");
        } else if (humanChoice.equals("paper")) {
            return computerChoice.equals("rock");
        } else {
            return computerChoice.equals("paper");
        }
        }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
